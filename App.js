import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  TextInput,
  ScrollView,
  StatusBar,
} from 'react-native';
import AIAFeedback from 'feedback-rn-sdk-dist';

const getFontSize = number => {
  return Platform.OS === 'android' ? number / 1.3 : number;
};

const App = props => {
  const feedbackRef = useRef();
  const [appVersion, setAppVersion] = useState('');
  const [translucent, setTranslucent] = useState(false);

  useEffect(() => {
    setupFeedbackSDK();
  }, []);

  // set translucent status bar
  const toggleTranslucent = () => {
    StatusBar.setTranslucent(!translucent);
    setTranslucent(!translucent);
  };

  useEffect(() => {
    setAppVersion(feedbackRef.current.getVersion());
  }, [feedbackRef]);

  const setupFeedbackSDK = () => {
    // Please contact us to obtain an App Secret specific to your Bundle ID/Package ID
    feedbackRef.current.setAppSecret('QcJasjiu2olMBxHyFohKJ');
  };

  const setLanguage = language => {
    feedbackRef.current.setLanguage(language);
  };

  const setAppUserId = () => {
    // This is an optional User ID provided by the host app.
    feedbackRef.current.setAppUserId('1234');
  };

  const setMetadata = () => {
    // This is an optional metadata provided by the host app.
    feedbackRef.current.setMetadata('policy_1234567890');
  };

  const setFormSlug = slug => {
    // This function can be called elsewhere inside your application code
    // However the mounting point <AIAFeedback ref={feedbackRef} /> should be directly in your App.js for it to stay on top of all other views
    feedbackRef.current.setFormSlug(slug);
  };

  const testRatingForm = eventTag => {
    // This will perform a request to our backend to obtain a form
    // This will always show the Feedback form for debugging purpose
    //feedbackRef.current.setFormSlug('native_rating_form');
    feedbackRef.current.testRatingForm(eventTag, formDidShow => {
      //This callback function is triggered when the SDK has finish its operation
      //When there is no form to be shown, this function is triggered almost immediately with formDidShow = false
      //formDidShow = true when there is a form and user has finish interacting with the form & dismiss it
      console.log('formDidShow', formDidShow);
    });
  };

  const showRatingForm = eventTag => {
    // This will perform a request to our backend to obtain a form,
    // However, it may not always show the form, depending on the Response Frequency & Prompt Frequency
    feedbackRef.current.showRatingForm(eventTag, formDidShow => {
      //This callback function is triggered when the SDK has finish its operation
      //When there is no form to be shown, this function is triggered almost immediately with formDidShow = false
      //formDidShow = true when there is a form and user has finish interacting with the form & dismiss it
      console.log('formDidShow', formDidShow);
    });
  };

  const languages = [
    //{title: 'English (EN)', slug: 'en'},
    {title: 'Simp. Chinese (zh)', slug: 'zh'},
    {title: 'Trad. Chinese (zh_tw)', slug: 'zh_tw'},
    {title: 'Tamil (ta)', slug: 'ta'},
    {title: 'Malay (ms)', slug: 'ms'},
    {title: 'Indonesian (id)', slug: 'id'},
    {title: 'Thai (th)', slug: 'th'},
    {title: 'Tagalog (tl)', slug: 'tl'},
    {title: 'Vietnamese (vi)', slug: 'vi'},
  ];

  /*
    Each slug is configured to show a different type of feedback form
    Please console your business team to obtain the values and timing in your user journey
  */
  const formSlugs = [
    {title: 'Satisfaction', slug: 'native_rating_form'},
    {title: 'NPS', slug: 'nps-1'},
    {title: 'Poll', slug: 'poll-1'},
    {title: 'Comment', slug: 'comment-1'},
    {title: 'CES', slug: 'effort-1'},
    {title: 'External Survey', slug: 'external-1'},
  ];

  function HomeScreen() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={{
            flex: 1,
          }}>
          <View style={{alignItems: 'center', marginBottom: 15}}>
            <Text style={styles.textDescription}>(Optional)</Text>
            <TouchableOpacity onPress={() => setLanguage('en')}>
              <Text style={styles.textAction}>
                Change language to English (EN)
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.viewWrap}>
            {languages.map(({title, slug}) => (
              <View key={`languages-${slug}`} style={styles.wrapItem}>
                <TouchableOpacity onPress={() => setLanguage(slug)}>
                  <Text style={styles.textAction}>{title}</Text>
                </TouchableOpacity>
              </View>
            ))}
          </View>
          <View style={{alignItems: 'center', marginBottom: 10}}>
            <Text style={styles.textDescription}>(Optional)</Text>
            <TouchableOpacity onPress={setAppUserId}>
              <Text style={styles.textAction}>Set your own User ID</Text>
            </TouchableOpacity>
          </View>
          <View style={{alignItems: 'center', marginBottom: 15}}>
            <TouchableOpacity onPress={setMetadata}>
              <Text style={styles.textAction}>Set your own metadata</Text>
            </TouchableOpacity>
          </View>
          <View style={{alignItems: 'center', marginBottom: 15}}>
            <Text style={styles.textDescription}>Test / Debug mode</Text>
            <View style={styles.viewWrap}>
              {formSlugs.map(({title, slug}) => (
                <View key={`formSlugs-${slug}`} style={styles.wrapItem}>
                  <TouchableOpacity
                    onPress={() => {
                      setFormSlug(slug);
                      testRatingForm('debug_event');
                    }}>
                    <Text style={styles.textAction}>{title}</Text>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
            <Text style={styles.textDescription}>
              {'Default input mode behavior'}
            </Text>
            <Text
              onPress={toggleTranslucent}
              style={[styles.textDescription, styles.translucent]}>
              Toggle StatusBar translucent (android)
            </Text>
            <TextInput
              style={styles.inputExample}
              placeholder={'Example keyboard input'}
              placeholderTextColor={'black'}
            />
          </View>
          <View style={{alignItems: 'center'}}>
            <Text style={styles.textDescription}>Production Mode</Text>
            <View style={styles.viewWrap}>
              {formSlugs.map(({title, slug}) => (
                <View key={`formSlugs-${slug}`} style={styles.wrapItem}>
                  <TouchableOpacity
                    onPress={() => {
                      setFormSlug(slug);
                      showRatingForm('login_event');
                    }}>
                    <Text style={styles.textAction}>{title}</Text>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>

          <View style={styles.viewVersion}>
            <Text style={styles.textDescription}>{appVersion}</Text>
          </View>
        </ScrollView>
      </View>
    );
  }

  function DummyTabContent() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Dummy Tab Content</Text>
      </View>
    );
  }


  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <HomeScreen />

      {/* Please insert this mount point directly in your App.js,
        preferably at the bottom of your root view tree for it to stay on top of all other views */}
      <AIAFeedback ref={feedbackRef} />
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: '#555555',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: '#fff',
    fontSize: getFontSize(18),
    marginTop: 5,
  },
  textAction: {
    color: '#007AFF',
    fontSize: getFontSize(18),
    marginTop: 5,
  },
  textDescription: {
    fontSize: getFontSize(16),
    color: '#EEEEEE',
  },
  viewWrap: {
    alignItems: 'center',
    marginBottom: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  viewVersion: {
    position: 'absolute',
    bottom: '3%',
    alignSelf: 'center',
  },
  wrapItem: {
    flexBasis: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 4,
  },
  inputExample: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
    marginVertical: 5,
    height: 40,
    paddingHorizontal: 10,
  },
  translucent: {
    borderWidth: 1,
    borderColor: 'white',
    marginVertical: 15,
    padding: 10,
    borderRadius: 10,
  },
});
